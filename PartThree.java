import java.util.Scanner;
public class PartThree {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter number 1: ");
		double number1 = reader.nextDouble();
		System.out.println("Enter number 2: ");
		double number2 = reader.nextDouble();
		System.out.println("Add: " + Calculator.Add(number1,number2));
		System.out.println("Substract: " + Calculator.Substract(number1,number2));
		Calculator sc = new Calculator();
		System.out.println("Multiply: " + sc.Multiply(number1, number2));
		System.out.println("Divide: " + sc.Divide(number1, number2));
	}
}