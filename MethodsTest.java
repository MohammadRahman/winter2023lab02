public class MethodsTest {
	
	public static void main(String[] args){
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x + 10);
		System.out.println(x);
		methodTwoInputNoReturn(3,3.5);
		int z = methodNoInputReturnInt();
		System.out.println("Variable z: " + z);
		double number = sumSquareRoot(9,5);
		System.out.println("Square root: " + number);
		String s1 = "java";
		String s2 = "programming";
		System.out.println("Length s1: " + s1.length());
		System.out.println("Length s2: " + s2.length());
		System.out.println("Add One: " + SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println("Add Two: " + sc.addTwo(50));
	}
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int number) {
		System.out.println("Inside the method one input no return");
		number = number - 5;
		System.out.println(number);
	}
	public static void methodTwoInputNoReturn(int a, double b) {
		System.out.println("Inside the method two input no return");
		System.out.println("Int: " + a + " double: " + b);
	}
	public static int methodNoInputReturnInt(){
	    int x = 5;
        return x;	
	}
	public static double sumSquareRoot(int num1, int num2) {
		int sum = num1 + num2;
		return Math.sqrt(sum);
	}
}